# What contains this repository?
This repository contains example project for non-official demo board W7500P-Tau.

This project is doing that:
1. It is blinking led on the board.
2. It is opening UDP-socket, that send message every 500 millisecund.

This is sending 4 bytes counter to 192.168.1.22::5270.

This contains python3 script for receiving data from UDP on PC.

Board IP is 192.168.1.69.

For more details refer to this [article](https://habr.com/ru/post/569960/).

# Building
```
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Debug-target ..
make all -j
```

# Debug
Embedded GDB Server:
```
'target remote' args: localhost:3333
GDB Server: pyocd
GDB Server args: gdbserver --target w7500 --connect halt
```

#include "main.h"

_Noreturn void eth_thread (__attribute__((unused)) const void *obj) {
    uint8_t phy_addr = 0;

    SET_ETH_CLK();

    if (!phy_init(&phy_addr)) {
        mc_reset();
    }

    while (!phy_is_link(phy_addr)) {
        vsrtos_delay(MS_TO_TICK(100));
    }

    SET_ETH_MAC(0x00, 0x08, 0xDC, 0x71, 0x72, 0x77);
    SET_ETH_IP(192, 168, 1, 69);
    SET_ETH_GW(192, 168, 1, 1);
    SET_ETH_MSK(255, 255, 255, 0);

    socket_udp_close(ETH_SOCKET_0);
    socket_udp_open(ETH_SOCKET_0, UDP_RX_PORT);

    while (true) {
        for (uint32_t i = 0; i < 1000; i++) {
            socket_udp_send(ETH_SOCKET_0, ETH_SOCKET_0_TX, &i, sizeof(i), SET_SOCKET_TX_IP(192, 168, 1, 22),
                            UDP_TX_PORT);
            vsrtos_delay(MS_TO_TICK(500));
        }
    }
}
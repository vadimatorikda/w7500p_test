#ifndef BALANCE_GPIO_H
#define BALANCE_GPIO_H

#include <stdint.h>

void init_led_pin ();
void init_miim_pins ();

void led_set_state (uint8_t state);

void mdio_idle ();
void mdio_out (uint32_t val, uint32_t n);
void mdio_turnaround ();
uint32_t mdio_in ();

#endif // BALANCE_GPIO_H
